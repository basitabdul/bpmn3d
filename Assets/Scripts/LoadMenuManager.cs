﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadMenuManager : MonoBehaviour {

	public Slider slider;

	void Start () {
		StartCoroutine(Wait());
	}

	IEnumerator Wait()
 	{
   		yield return new WaitForSeconds(3);

 		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
	}

	void Update() {
		slider.value = Time.time / 3;
	}
	
}
