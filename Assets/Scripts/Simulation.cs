﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class Simulation : MonoBehaviour {

	public List<Process> process;
	public List<Participant> participants;
	public List<Token> tokens = new List<Token> ();

	public Button simulationButton;
	public Button pauseButton;
	public Button exitButton;

	public bool simulationEnded = false;

	void Start () {

		GameObject _canvas = GameObject.Find("Canvas");
		FileLoader fileLoader = _canvas.GetComponent<FileLoader> ();

		process = fileLoader.GetProcess ();
		participants = fileLoader.GetParticipants ();
		tokens = fileLoader.GetTokens ();

	}
	
	public void StartSimulation() 
	{
		foreach (Token token in tokens) {
			token.TokenMovement.MoveToken (token);
		}
		simulationButton.gameObject.SetActive(false);
		pauseButton.gameObject.SetActive(true);
		exitButton.gameObject.SetActive(true);
	}

	
}
