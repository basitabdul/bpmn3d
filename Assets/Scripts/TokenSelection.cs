﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TokenSelection : MonoBehaviour {

	private List<GameObject> models;
	private int selectionIndex = 0;
	FileLoader fileLoader;

	// Use this for initialization
	void Start () {
		GameObject _canvas = GameObject.Find("Canvas");
        fileLoader = _canvas.GetComponent<FileLoader>();

		models = new List<GameObject>();
		foreach(Transform t in transform)
		{
			models.Add(t.gameObject);
			t.gameObject.SetActive(false);
		}

		fileLoader.SetTokenModels(models);

		models[selectionIndex].SetActive(true);	
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetMouseButton(0))
		{
			transform.Rotate(new Vector3(0.0f, Input.GetAxis("Mouse X"), 0.0f));
		}
	}

	public void Right() 
	{
		int index = selectionIndex;
		selectionIndex++;
		if(selectionIndex >= models.Count)
		{
			selectionIndex = 0;
		}
		
		models[index].SetActive(false);
		models[selectionIndex].SetActive(true);

		fileLoader.setTokenSelectionIndex(selectionIndex);
	}

	public void Left() 
	{
		int index = selectionIndex;
		selectionIndex--;
		if(selectionIndex < 0)
		{
			selectionIndex = models.Count - 1;
		}
		
		models[index].SetActive(false);
		models[selectionIndex].SetActive(true);

		fileLoader.setTokenSelectionIndex(selectionIndex);
	}

	public List<GameObject> ListOfModels() 
	{
		return models;
	}

}
