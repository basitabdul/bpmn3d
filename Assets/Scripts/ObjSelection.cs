﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjSelection : MonoBehaviour {

	private List<GameObject> models;
	private int selectionIndex = 0;
	FileLoader fileLoader;

	// Use this for initialization
	void Start () 
	{
		GameObject _canvas = GameObject.Find("Canvas");
        fileLoader = _canvas.GetComponent<FileLoader>();

		models = new List<GameObject>();
		foreach(Transform t in transform)
		{
			models.Add(t.gameObject);
			t.gameObject.SetActive(false);
		}

		fileLoader.SetModels(models);

		models[selectionIndex].SetActive(true);
	}

	void Update() 
	{
		if(Input.GetMouseButton(0))
		{
			transform.Rotate(new Vector3(0.0f, Input.GetAxis("Mouse X"), 0.0f));
		}
	}

	public void Right() 
	{
		int index = selectionIndex;
		selectionIndex++;
		if(selectionIndex >= models.Count)
		{
			selectionIndex = 0;
		}
		
		models[index].SetActive(false);
		models[selectionIndex].SetActive(true);

		fileLoader.setSelectionIndex(selectionIndex);
	}

	public void Left() 
	{
		int index = selectionIndex;
		selectionIndex--;
		if(selectionIndex < 0)
		{
			selectionIndex = models.Count - 1;
		}
		
		models[index].SetActive(false);
		models[selectionIndex].SetActive(true);

		fileLoader.setSelectionIndex(selectionIndex);
	}
	
	/*public void Select(int _index) 
	{
		if(_index == selectionIndex)
		{
			return;
		}

		if(_index < 0 || _index >= models.Count)
		{
			return;
		}

		models[selectionIndex].SetActive(false);
		selectionIndex = _index;
		models[selectionIndex].SetActive(true);

		fileLoader.setSelectionIndex(selectionIndex);
	}
*/
	public List<GameObject> ListOfModels() 
	{
		return models;
	}
}
