﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using System.Linq;
using System;

//[RequireComponent(typeof(NavMeshAgent))]
public class TokenMovement : MonoBehaviour
{

    //NavMeshAgent agent;
    public List<Process> process;
    NavMeshAgent agent;
    bool hasFinished = false;
    Token token;
    bool isWorking = false;
    FileLoader fileLoader;
    string nodeChosenByUser = null;
    public GameObject ChooseNode;
    bool isWaiting = false;

    int count = 0;

    GameObject arrowGO;
    Animator animator;

    void Start()
    {
        GameObject _canvas = GameObject.Find("Canvas");
        fileLoader = _canvas.GetComponent<FileLoader>();
        process = fileLoader.GetProcess();

        arrowGO = GameObject.Find("Arrow");

        animator = gameObject.GetComponentInChildren<Animator>();
    }

    void Update()
    {
        if (token != null)
        {
            if (!hasFinished && !token.IsPaused)
            {
                if (agent != null)
                {
                    if (!agent.pathPending && agent.remainingDistance < 0.5f)
                    {
                        if (!isWorking && !isWaiting)
                        {
                            StartCoroutine(WaitForThreeSeconds());
                        }
                    }
                }
            }
        }

    }

    IEnumerator WaitForThreeSeconds()
    {
        //making the token face the current node
        /*var xPosition = fileLoader.GetXPosition(token.NextNodeId);
        var yPosition = fileLoader.GetYPosition(token.NextNodeId);

        if (xPosition != null && yPosition != null)
        {
            Vector3 newPosition = new Vector3(int.Parse(xPosition) / 10, 0f, int.Parse(yPosition) / 10);
            token.Obj.transform.rotation = Quaternion.LookRotation(newPosition);
        }*/

        isWorking = true;
        if (animator != null)
        {
            animator.SetBool("IsWalking", false);
        }

        yield return new WaitForSeconds(3);

        if (animator != null)
        {
            animator.SetBool("IsWalking", true);
        }

        token.PreviousNodeId = token.CurrentNodeId;
        token.CurrentNodeId = token.NextNodeId;
        token.NextNodeId = null;

        token.XPosition = fileLoader.GetXPosition(token.CurrentNodeId);
        token.YPosition = fileLoader.GetYPosition(token.CurrentNodeId);

        MoveToken(token);

        isWorking = false;
    }

    public void MoveToken(Token _token)
    {
        agent = _token.Agent;
        token = _token;

        List<string> outgoings = new List<string>();
        Dictionary<string, bool> incomings = new Dictionary<string, bool>();

        if (_token.NextNodeId == null)
        {
            string typeOfNode = GetType(_token.CurrentNodeId);
            outgoings = FindNextNode(_token.CurrentNodeId);
            if (typeOfNode == "StartEvent")
            {
                int numberOfIncomings = GetNumberOfTokensIncoming(_token.CurrentNodeId);
                if (numberOfIncomings > 0)
                {
                    if (_token.PreviousNodeId != null)
                    {
                        fileLoader.SetTokenArrived(_token.CurrentNodeId, _token.PreviousNodeId);
                    }
                    int numberOfTokensArrived = GetNumberOfTokensArrived(_token.CurrentNodeId);
                    if (numberOfIncomings == numberOfTokensArrived)
                    {
                        Vector3 pos;
                        NavMeshAgent _agent;
                        TokenMovement _tokenMovement;
                        pos.x = int.Parse(_token.XPosition.Split('.')[0]) / 10;
                        pos.z = int.Parse(_token.YPosition.Split('.')[0]) / 10;
                        pos.y = 0.83f;
                        Quaternion startSpawnRotation = GameObject.FindGameObjectWithTag("StartEventSpawn").transform.rotation;

                        //GameObject duplicate = Instantiate(Resources.Load("Token"), pos, startSpawnRotation) as GameObject;
                        GameObject duplicate = Instantiate(fileLoader.GetChosenToken(), pos, startSpawnRotation) as GameObject;

                        duplicate.name = "Token" + GetProcessId(_token.CurrentNodeId);
                        _agent = duplicate.GetComponent<NavMeshAgent>();
                        _tokenMovement = duplicate.GetComponent<TokenMovement>();
                        /*NavMeshAgent _agent;
                        TokenMovement _tokenMovement;
                        GameObject duplicate = Instantiate(_token.Obj) as GameObject;
                        duplicate.name = "Token" + GetProcessId(_token.CurrentNodeId);
                        _agent = duplicate.GetComponent<NavMeshAgent>();
                        _tokenMovement = duplicate.GetComponent<TokenMovement>();*/

                        Token duplicatedToken = new Token();
                        duplicatedToken.Id = GetProcessId(_token.CurrentNodeId);
                        duplicatedToken.Name = "Token" + duplicatedToken.Id;
                        duplicatedToken.XPosition = _token.XPosition;
                        duplicatedToken.YPosition = _token.YPosition;
                        duplicatedToken.CurrentNodeId = _token.CurrentNodeId;
                        duplicatedToken.NextNodeId = outgoings.ElementAt(0);
                        duplicatedToken.Agent = _agent;
                        duplicatedToken.TokenMovement = _tokenMovement;
                        duplicatedToken.Obj = duplicate;
                        duplicatedToken.OriginalTokenId = _token.Id;
                        duplicatedToken.TypeOfToken = TypeOfToken.Token;

                        _token.AddDuplicate(duplicatedToken);
                        fileLoader.AddToken(duplicatedToken);

                        Vector3 point = FindPoint(duplicatedToken.NextNodeId);

                        Animator anim = duplicatedToken.Obj.GetComponentInChildren<Animator>();
                        anim.SetBool("IsWalking", true);

                        MoveToPoint(point, duplicatedToken);

                        duplicatedToken.TokenMovement.MoveToken(duplicatedToken);
                    }

                    if (_token.TypeOfToken == TypeOfToken.Message)
                    {
                        _token.Obj.SetActive(false);
                    }
                }
                else
                {
                    _token.NextNodeId = outgoings.ElementAt(0);

                    if (_token.NextNodeId != null)
                    {
                        Vector3 point = FindPoint(_token.NextNodeId);
                        MoveToPoint(point, _token);
                    }
                    else
                    {
                        hasFinished = true;
                    }
                }
            }
            else if (typeOfNode == "Task")
            {
                _token.NextNodeId = outgoings.ElementAt(0);

                if (_token.NextNodeId != null)
                {
                    Vector3 point = FindPoint(_token.NextNodeId);
                    MoveToPoint(point, _token);
                }
                else
                {
                    hasFinished = true;
                }
            }
            else if (typeOfNode == "ExclusiveGateway")
            {
                if (outgoings.Count > 1)
                {
                    isWaiting = true;
                    foreach (string id in outgoings)
                    {
                        //Create an arrow and set the name of the arrow as the next node id
                        //GameObject arrow = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                        GameObject arrow = GameObject.Instantiate(arrowGO);
                        arrow.name = id;

                        arrow.AddComponent<ChooseNode>();

                        arrow.GetComponent<ChooseNode>().ConnectTokenMovement(this, _token);

                        Vector3 point = FindPoint(id);

                        Vector3 myPosition = FindPoint(_token.CurrentNodeId);

                        Vector3 arrowPosition = (point - myPosition) / 5.0f;
                        arrowPosition += myPosition;

                        arrowPosition.y += 1.0f;

                        arrow.transform.position = arrowPosition;

                        //The arrow model is orieted in wrong direction
                        //so I am rotating it of 180 degrees
                        arrow.transform.LookAt(point);
                        Vector3 lookingPoint = arrow.transform.rotation.eulerAngles;
                        lookingPoint = new Vector3(lookingPoint.x + 180, lookingPoint.y, lookingPoint.z);
                        arrow.transform.rotation = Quaternion.Euler(lookingPoint);

                    }

                    StartCoroutine(WaitForUsersChoice());
                }
                else
                {
                    _token.NextNodeId = outgoings[0];

                    if (_token.NextNodeId != null)
                    {
                        Vector3 point = FindPoint(_token.NextNodeId);
                        MoveToPoint(point, _token);
                    }
                    else
                    {
                        hasFinished = true;
                    }
                }




                /*System.Random rand = new System.Random();

                int position = rand.Next(outgoings.Count);

                _token.NextNodeId = outgoings[position];

                if (_token.NextNodeId != null)
                {
                    Vector3 point = FindPoint(_token.NextNodeId);
                    MoveToPoint(point, _token);
                }
                else
                {
                    hasFinished = true;
                }*/
            }
            else if (typeOfNode == "ParallelGateway")
            {
                if (outgoings.Count > 1)
                {
                    int countClone = 0;
                    foreach (string id in outgoings)
                    {
                        NavMeshAgent _agent;
                        TokenMovement _tokenMovement;
                        GameObject duplicate = Instantiate(_token.Obj);
                        _agent = duplicate.GetComponent<NavMeshAgent>();
                        _tokenMovement = duplicate.GetComponent<TokenMovement>();

                        Token duplicatedToken = new Token();
                        countClone++;
                        //To be changed
                        duplicatedToken.Id = countClone + 100;
                        duplicatedToken.Name = "Clone" + countClone + "OfToken" + _token.Id;
                        duplicatedToken.XPosition = _token.XPosition;
                        duplicatedToken.YPosition = _token.YPosition;
                        duplicatedToken.CurrentNodeId = _token.CurrentNodeId;
                        duplicatedToken.NextNodeId = id;
                        duplicatedToken.Agent = _agent;
                        duplicatedToken.TokenMovement = _tokenMovement;
                        duplicatedToken.Obj = duplicate;
                        duplicatedToken.OriginalTokenId = _token.Id;
                        duplicatedToken.TypeOfToken = TypeOfToken.Clone;
                        duplicatedToken.ButtonHasBeenCreated = false;
                        duplicatedToken.IsButtonActive = true;

                        _token.AddDuplicate(duplicatedToken);
                        fileLoader.AddToken(duplicatedToken);

                        Vector3 point = FindPoint(duplicatedToken.NextNodeId);
                        MoveToPoint(point, duplicatedToken);
                    }
                    //Disattivo il token originale
                    _token.Obj.SetActive(false);
                    fileLoader.DisableButton(_token.Id);
                    //_token.IsButtonActive = false;

                    foreach (Token t in _token.DuplicatedTokens)
                    {
                        t.TokenMovement.MoveToken(t);
                    }

                }
                else
                {
                    foreach (Token t in fileLoader.tokens)
                    {
                        if (t.Id == _token.OriginalTokenId)
                        {
                            if (t.DuplicatedTokens.Count > 1)
                            {
                                var itemToRemove = t.DuplicatedTokens.Single(r => r.Id == _token.Id);
                                t.DuplicatedTokens.Remove(itemToRemove);

                                //Destroy(_token.Obj);
                                fileLoader.DeleteButton(_token.Id);
                                _token.Obj.SetActive(false);
                            }
                            else if (t.DuplicatedTokens.Count == 1)
                            {
                                //Elimino il token dalla lista dei dupplicati del token originale
                                var itemToRemove = t.DuplicatedTokens.Single(tok => tok.Id == _token.Id);
                                t.DuplicatedTokens.Remove(itemToRemove);

                                List<string> newNextNodeId = FindNextNode(_token.CurrentNodeId);

                                Token originalToken = fileLoader.GetToken(_token.OriginalTokenId);
                                Vector3 newPosition;
                                newPosition.x = int.Parse(_token.XPosition.Split('.')[0]) / 10;
                                newPosition.z = int.Parse(_token.YPosition.Split('.')[0]) / 10;
                                newPosition.y = 0.83f;
                                originalToken.Obj.SetActive(true);
                                originalToken.IsButtonActive = true;
                                fileLoader.ActiveButton(originalToken.Id);
                                _token.IsButtonActive = false;
                                originalToken.Obj.transform.position = newPosition;
                                originalToken.NextNodeId = newNextNodeId[0];
                                Vector3 point = FindPoint(newNextNodeId[0]);

                                MoveToPoint(point, originalToken);

                                //Distruggo l'ultimo duplicato del token originale
                                _token.Obj.SetActive(false);
                                fileLoader.DeleteButton(_token.Id);

                                originalToken.TokenMovement.MoveToken(originalToken);

                            }
                        }


                    }

                }
            }
            else if (typeOfNode == "SendTask")
            {
                //salvo l'id del mio processo in una variabile
                //per ogni outgoings controllo l'id del processo del task
                //se i due id corrispondono ---> spedisco il token originale a destinazione
                //se i due id non corrispondono ---> creo una copia del token e lo spedisco a destinazione
                int? currentProcessId = GetProcessId(_token.CurrentNodeId);

                foreach (string id in outgoings)
                {
                    int? nextProcessId = GetProcessId(id);

                    if (currentProcessId == nextProcessId)
                    {
                        _token.NextNodeId = id;

                        if (_token.NextNodeId != null)
                        {
                            Vector3 point = FindPoint(_token.NextNodeId);
                            MoveToPoint(point, _token);
                        }
                        else
                        {
                            hasFinished = true;
                        }
                    }
                    else
                    {
                        Vector3 pos;
                        NavMeshAgent _agent;
                        TokenMovement _tokenMovement;
                        pos.x = int.Parse(_token.XPosition.Split('.')[0]) / 10; ;
                        pos.z = int.Parse(_token.YPosition.Split('.')[0]) / 10;
                        pos.y = 0.83f;
                        Quaternion startSpawnRotation = GameObject.FindGameObjectWithTag("StartEventSpawn").transform.rotation;

                        GameObject duplicate = Instantiate(Resources.Load("Message"), pos, startSpawnRotation) as GameObject;
                        duplicate.name = "Message" + GetProcessId(_token.CurrentNodeId);
                        _agent = duplicate.GetComponent<NavMeshAgent>();
                        _tokenMovement = duplicate.GetComponent<TokenMovement>();

                        Token duplicatedToken = new Token();
                        duplicatedToken.Id = 99;
                        duplicatedToken.Name = _token.Name;
                        duplicatedToken.XPosition = _token.XPosition;
                        duplicatedToken.YPosition = _token.YPosition;
                        duplicatedToken.CurrentNodeId = _token.CurrentNodeId;
                        duplicatedToken.NextNodeId = id;
                        duplicatedToken.Agent = _agent;
                        duplicatedToken.TokenMovement = _tokenMovement;
                        duplicatedToken.Obj = duplicate;
                        duplicatedToken.OriginalTokenId = _token.Id;
                        duplicatedToken.TypeOfToken = TypeOfToken.Message;


                        fileLoader.AddToken(duplicatedToken);

                        Vector3 point = FindPoint(duplicatedToken.NextNodeId);
                        MoveToPoint(point, duplicatedToken);

                        duplicatedToken.TokenMovement.MoveToken(duplicatedToken);
                    }
                }
            }
            else if (typeOfNode == "ReceiveTask")
            {
                //Appena arriva un token setto l'incoming a true
                //controllo se sono arrivati tutti i token dai incomings
                //nel caso positivo creo il token per il processo e lo spedisco.

                fileLoader.SetTokenArrived(_token.CurrentNodeId, _token.PreviousNodeId);

                int numberOfTokensArrived = GetNumberOfTokensArrived(_token.CurrentNodeId);
                int numberOfTokensIncoming = GetNumberOfTokensIncoming(_token.CurrentNodeId);

                if (numberOfTokensArrived == numberOfTokensIncoming)
                {

                    Token pausedToken = fileLoader.GetPausedToken(_token.CurrentNodeId);

                    pausedToken.NextNodeId = outgoings.ElementAt(0);
                    animator = pausedToken.Obj.GetComponentInChildren<Animator>();
                    if (pausedToken.NextNodeId != null)
                    {
                        Vector3 point = FindPoint(pausedToken.NextNodeId);
                        if(animator != null)
                        {
                            animator.SetBool("IsWalking", true);
                        }
                        
                        MoveToPoint(point, pausedToken);
                    }
                    else
                    {
                        hasFinished = true;
                    }

                    pausedToken.IsPaused = false;

                    pausedToken.TokenMovement.MoveToken(pausedToken);

                    if (_token.TypeOfToken == TypeOfToken.Message)
                    {
                        _token.Obj.SetActive(false);
                    }
                }
                else
                {
                    token.IsPaused = true;

                    if (animator != null)
                    {
                        animator.SetBool("IsWalking", false);
                    }

                    fileLoader.pausedTokens.Add(new PausedToken()
                    {
                        NodeId = _token.CurrentNodeId,
                        Token = _token
                    });
                }
            }
            else if (typeOfNode == "EventBasedGateway")
            {
                if (outgoings.Count > 1)
                {
                    int countClone = 0;
                    foreach (string id in outgoings)
                    {
                        NavMeshAgent _agent;
                        TokenMovement _tokenMovement;
                        GameObject duplicate = Instantiate(_token.Obj);
                        _agent = duplicate.GetComponent<NavMeshAgent>();
                        _tokenMovement = duplicate.GetComponent<TokenMovement>();

                        Token duplicatedToken = new Token();
                        countClone++;
                        duplicatedToken.Id = countClone + 100;
                        duplicatedToken.Name = "Clone" + countClone + "OfToken" + _token.Id;
                        duplicatedToken.XPosition = _token.XPosition;
                        duplicatedToken.YPosition = _token.YPosition;
                        duplicatedToken.CurrentNodeId = _token.CurrentNodeId;
                        duplicatedToken.NextNodeId = id;
                        duplicatedToken.Agent = _agent;
                        duplicatedToken.TokenMovement = _tokenMovement;
                        duplicatedToken.Obj = duplicate;
                        duplicatedToken.OriginalTokenId = _token.Id;
                        duplicatedToken.TypeOfToken = TypeOfToken.Clone;
                        duplicatedToken.ButtonHasBeenCreated = false;
                        duplicatedToken.IsButtonActive = true;

                        _token.AddDuplicate(duplicatedToken);
                        fileLoader.AddToken(duplicatedToken);

                        Vector3 point = FindPoint(duplicatedToken.NextNodeId);

                        Animator anim = duplicatedToken.Obj.GetComponentInChildren<Animator>();
                        anim.SetBool("IsWalking", true);
                        MoveToPoint(point, duplicatedToken);
                    }
                    //Disattivo il token originale
                    _token.Obj.SetActive(false);
                    fileLoader.DisableButton(_token.Id);
                    //_token.IsButtonActive = false;

                    foreach (Token t in _token.DuplicatedTokens)
                    {
                        t.TokenMovement.MoveToken(t);
                    }

                }
                else
                {
                    _token.NextNodeId = outgoings.ElementAt(0);

                    if (_token.NextNodeId != null)
                    {
                        Vector3 point = FindPoint(_token.NextNodeId);
                        MoveToPoint(point, _token);
                    }
                    else
                    {
                        hasFinished = true;
                    }
                }
            }
            else if (typeOfNode == "IntermediateCatchEvent")
            {
                //Appena arriva un token setto l'incoming a true
                //controllo se sono arrivati tutti i token dai incomings
                //nel caso positivo creo il token per il processo e lo spedisco.

                fileLoader.SetTokenArrived(_token.CurrentNodeId, _token.PreviousNodeId);

                int numberOfTokensArrived = GetNumberOfTokensArrived(_token.CurrentNodeId);
                int numberOfTokensIncoming = GetNumberOfTokensIncoming(_token.CurrentNodeId);

                if (numberOfTokensArrived == numberOfTokensIncoming)
                {

                    Token pausedToken = fileLoader.GetPausedToken(_token.CurrentNodeId);

                    pausedToken.NextNodeId = outgoings.ElementAt(0);

                    animator = pausedToken.Obj.GetComponentInChildren<Animator>();

                    if (pausedToken.NextNodeId != null)
                    {
                        Vector3 point = FindPoint(pausedToken.NextNodeId);

                        if(animator != null)
                        {
                            animator.SetBool("IsWalking", true);
                        }

                        MoveToPoint(point, pausedToken);
                    }
                    else
                    {
                        hasFinished = true;
                    }

                    pausedToken.IsPaused = false;

                    pausedToken.TokenMovement.MoveToken(pausedToken);

                    if (_token.TypeOfToken == TypeOfToken.Message)
                    {
                        _token.Obj.SetActive(false);
                    }
                }
                else
                {
                    token.IsPaused = true;

                    if (animator != null)
                    {
                        animator.SetBool("IsWalking", false);
                    }

                    fileLoader.pausedTokens.Add(new PausedToken()
                    {
                        NodeId = _token.CurrentNodeId,
                        Token = _token
                    });
                }
                /*fileLoader.SetTokenArrived(_token.CurrentNodeId, _token.PreviousNodeId);

                int numberOfTokensArrived = GetNumberOfTokensArrived(_token.CurrentNodeId);
                int numberOfTokensIncoming = GetNumberOfTokensIncoming(_token.CurrentNodeId);

                if (numberOfTokensArrived == numberOfTokensIncoming)
                {
                    //Move the token to next node
                    _token.NextNodeId = outgoings.ElementAt(0);

                    if (_token.NextNodeId != null)
                    {
                        Vector3 point = FindPoint(_token.NextNodeId);
                        MoveToPoint(point, _token);
                    }
                    else
                    {
                        hasFinished = true;
                    }
                }
                else
                {
                    //Get Previous node
                    //Move Token from previous node to current;
                    string previouseNodeId = fileLoader.GetPreviousNodeId(_token.CurrentNodeId);

                    Token pausedToken = fileLoader.GetPausedToken(previouseNodeId);

                    pausedToken.NextNodeId = _token.CurrentNodeId;

                    if (pausedToken.NextNodeId != null)
                    {
                        Vector3 point = FindPoint(pausedToken.NextNodeId);
                        MoveToPoint(point, pausedToken);
                    }
                    else
                    {
                        hasFinished = true;
                    }

                    pausedToken.TokenMovement.MoveToken(pausedToken);

                }*/
            }
            else if (typeOfNode == "IntermediateThrowEvent")
            {
                //salvo l'id del mio processo in una variabile
                //per ogni outgoings controllo l'id del processo del task
                //se i due id corrispondono ---> spedisco il token originale a destinazione
                //se i due id non corrispondono ---> creo una copia del token e lo spedisco a destinazione
                int? currentProcessId = GetProcessId(_token.CurrentNodeId);

                foreach (string id in outgoings)
                {
                    int? nextProcessId = GetProcessId(id);

                    if (currentProcessId == nextProcessId)
                    {
                        _token.NextNodeId = id;

                        if (_token.NextNodeId != null)
                        {
                            Vector3 point = FindPoint(_token.NextNodeId);
                            MoveToPoint(point, _token);
                        }
                        else
                        {
                            hasFinished = true;
                        }
                    }
                    else
                    {
                        Vector3 pos;
                        NavMeshAgent _agent;
                        TokenMovement _tokenMovement;
                        pos.x = int.Parse(_token.XPosition.Split('.')[0]) / 10; ;
                        pos.z = int.Parse(_token.YPosition.Split('.')[0]) / 10;
                        pos.y = 0.83f;
                        Quaternion startSpawnRotation = GameObject.FindGameObjectWithTag("StartEventSpawn").transform.rotation;

                        GameObject duplicate = Instantiate(Resources.Load("Message"), pos, startSpawnRotation) as GameObject;
                        duplicate.name = "Message" + GetProcessId(_token.CurrentNodeId);
                        _agent = duplicate.GetComponent<NavMeshAgent>();
                        _tokenMovement = duplicate.GetComponent<TokenMovement>();

                        Token duplicatedToken = new Token();
                        duplicatedToken.Id = 9;
                        duplicatedToken.Name = _token.Name;
                        duplicatedToken.XPosition = _token.XPosition;
                        duplicatedToken.YPosition = _token.YPosition;
                        duplicatedToken.CurrentNodeId = _token.CurrentNodeId;
                        duplicatedToken.NextNodeId = id;
                        duplicatedToken.Agent = _agent;
                        duplicatedToken.TokenMovement = _tokenMovement;
                        duplicatedToken.Obj = duplicate;
                        duplicatedToken.OriginalTokenId = _token.Id;
                        duplicatedToken.TypeOfToken = TypeOfToken.Message;

                        fileLoader.AddToken(duplicatedToken);

                        Vector3 point = FindPoint(duplicatedToken.NextNodeId);
                        MoveToPoint(point, duplicatedToken);

                        duplicatedToken.TokenMovement.MoveToken(duplicatedToken);
                    }
                }
            }
        }

    }

    public int GetNumberOfTokensIncoming(string _id)
    {
        int count = 0;
        foreach (Process _p in fileLoader.process)
        {
            foreach (Node _node in _p.Nodes)
            {
                if (_node.Id == _id)
                {
                    foreach (var item in _node.Incomings)
                    {
                        count++;
                    }
                }
            }
        }
        return count;
    }

    private int GetNumberOfTokensArrived(string _id)
    {
        int count = 0;
        foreach (Process _p in fileLoader.process)
        {
            foreach (Node _node in _p.Nodes)
            {
                if (_node.Id == _id)
                {
                    foreach (var item in _node.Incomings)
                    {
                        if (item.Value == true)
                        {
                            count++;
                        }
                    }
                }
            }
        }
        return count;
    }

    private string GetType(string _id)
    {
        foreach (Process process in process)
        {
            foreach (Node node in process.Nodes)
            {
                if (node.Id == _id)
                {
                    return node.Type.ToString();
                }
            }
        }
        return null;
    }


    private List<string> FindNextNode(string _currentNode)
    {
        foreach (Process process in process)
        {
            foreach (Node node in process.Nodes)
            {
                if (node.Id == _currentNode)
                {
                    return node.Outgoings;
                }
            }
        }
        return null;
    }

    private Vector3 FindPoint(string _id)
    {
        foreach (Process process in process)
        {
            foreach (Node node in process.Nodes)
            {
                if (node.Id == _id)
                {
                    Vector3 point = new Vector3(int.Parse(node.XPosition) / 10, 0, int.Parse(node.YPosition) / 10);
                    return point;
                }
            }
        }
        return Vector3.zero;
    }

    private void MoveToPoint(Vector3 point, Token token)
    {
        /*if (token.Id == 1)
        {
            count++;
            Debug.Log("sono stato chiamato " + token.Id + " " + count);
        }*/


        //In this way the token will not enter inside the 3d model 
        //Vector3 newPoint = new Vector3(point.x - 2, point.y, point.z);

        if (animator != null)
        {
            animator.SetBool("IsWalking", true);
        }

        Vector3 newPoint = new Vector3(point.x, point.y, point.z);
        token.Agent.SetDestination(newPoint);
        token.Obj.transform.rotation = Quaternion.LookRotation(newPoint);
        isWaiting = false;
    }

    private int? GetProcessId(string _id)
    {
        foreach (Process p in process)
        {
            foreach (Node _node in p.Nodes)
            {
                if (_node.Id == _id)
                {
                    return p.Number;
                }
            }
        }
        return null;
    }

    public void SetNodeChosenByUser(string _id, Token _token)
    {
        if (nodeChosenByUser == null)
        {
            nodeChosenByUser = _id;
            token = _token;
        }
    }

    IEnumerator WaitForUsersChoice()
    {
        while (true)
        {
            if (nodeChosenByUser != null)
            {
                token.NextNodeId = nodeChosenByUser;

                if (token.NextNodeId != null)
                {
                    Vector3 point = FindPoint(token.NextNodeId);
                    MoveToPoint(point, token);
                    nodeChosenByUser = null;
                    yield break;
                }
                else
                {
                    hasFinished = true;
                    yield break;
                }
            }
            yield return null;
        }

    }

}
