﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TokenButton 
{
	public int? Id { get; set; }
	public string Name { get; set; }
	public bool HasBeenCreated { get; set;}
	public GameObject TokenButtonObject { get; set; }
	public bool IsClone { get; set; }
	
}
