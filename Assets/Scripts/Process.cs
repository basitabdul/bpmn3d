﻿using System.Collections.Generic;

public class Process {

	public string Id { get; set; }
	public int Number { get; set; }
	public List<Node> Nodes = new List<Node>(); 


	public void AddNode(string _id, string _name, List<string> _outgoings, TypeOfNode _type, Dictionary<string, bool> _incomings) {
		Nodes.Add (new Node () {
			Id = _id,
			Name = _name,
			Outgoings = _outgoings,
			Type = _type,
			Incomings = _incomings
		});
	}

}
