﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChooseNode : MonoBehaviour {

	TokenMovement tokenMovement;
	Token token;

	void OnMouseDown()
	{
		tokenMovement.SetNodeChosenByUser(gameObject.name, token);
	}

	public void ConnectTokenMovement(TokenMovement _tokenMovement, Token _token)
	{
		tokenMovement = _tokenMovement;
		token = _token;
	}
}
