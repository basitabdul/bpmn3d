﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

//This class takes in input two points and draws a line between the two points
public class DrawLine {

	public void Draw(Vector3 _firstPoint, Vector3 _secondPoint, Material _material)
	{
		GameObject line = new GameObject();
		
		LineRenderer lineRenderer = line.AddComponent<LineRenderer>();

		lineRenderer.startWidth = 0.1f;
		lineRenderer.endWidth = 0.1f;

		lineRenderer.material = _material;

		lineRenderer.SetPosition(0, _firstPoint);
		lineRenderer.SetPosition(1, _secondPoint);

		lineRenderer.shadowCastingMode = ShadowCastingMode.Off;
	}

}
